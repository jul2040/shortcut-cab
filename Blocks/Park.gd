extends Spatial

export(Color) var color = Color(0.129887, 0.617188, 0.060272)
var minimap
const BUILDING_SIZE = 20
const BLOCK_SIZE = 5*BUILDING_SIZE
const MINIMAP_SCALE = 1
func generate():
	minimap = Image.new()
	minimap.create((BLOCK_SIZE+BUILDING_SIZE), (BLOCK_SIZE+BUILDING_SIZE), false, Image.FORMAT_RGBA8)
	minimap.lock()
	minimap.fill(color)
	var r = randi()%4
	if(r == 1):
		$Park.rotation.y = PI/2
	elif(r == 2):
		$Park.rotation.y = PI
	elif(r == 3):
		$Park.rotation.y = (3*PI)/2
	minimap.unlock()
	var offset = (BUILDING_SIZE/2)+3
	var smooth = 1
	#pedestrian curve
	$PedestrianPath.curve.add_point(Vector3(-offset, 0, -offset+3), Vector3(), Vector3(0, 0, -smooth))
	$PedestrianPath.curve.add_point(Vector3(-offset+3, 0, -offset), Vector3(-smooth, 0, 0), Vector3())
	
	$PedestrianPath.curve.add_point(Vector3(BLOCK_SIZE*BUILDING_SIZE+offset-3,0,-offset), Vector3(), Vector3(smooth, 0, 0))
	$PedestrianPath.curve.add_point(Vector3(BLOCK_SIZE*BUILDING_SIZE+offset,0,-offset+3), Vector3(0, 0, -smooth), Vector3())
	
	$PedestrianPath.curve.add_point(Vector3(BLOCK_SIZE*BUILDING_SIZE+offset,0,BLOCK_SIZE*BUILDING_SIZE+offset-3), Vector3(), Vector3(0, 0, smooth))
	$PedestrianPath.curve.add_point(Vector3(BLOCK_SIZE*BUILDING_SIZE+offset-3,0,BLOCK_SIZE*BUILDING_SIZE+offset), Vector3(smooth, 0, 0), Vector3())
	
	$PedestrianPath.curve.add_point(Vector3(-offset+3,0,BLOCK_SIZE*BUILDING_SIZE+offset), Vector3(), Vector3(-smooth, 0, 0))
	$PedestrianPath.curve.add_point(Vector3(-offset,0,BLOCK_SIZE*BUILDING_SIZE+offset-3), Vector3(0, 0, smooth), Vector3())
	
	$PedestrianPath.curve.add_point(Vector3(-offset, 0, -offset+3))
	
	#pedestrian area
	$PedestrianArea.translation.x = (BLOCK_SIZE*BUILDING_SIZE)/2
	$PedestrianArea.translation.z = (BLOCK_SIZE*BUILDING_SIZE)/2
	$PedestrianArea/CollisionShape.shape.extents.x = (BLOCK_SIZE*BUILDING_SIZE)
	$PedestrianArea/CollisionShape.shape.extents.z = (BLOCK_SIZE*BUILDING_SIZE)

func place_pedestrian(visual, unit_offset):
	var pedestrian = preload("res://Pedestrians/Pedestrian.tscn").instance()
	pedestrian.passenger = visual
	$PedestrianPath.add_car(pedestrian, null, unit_offset)

func get_minimap():
	return minimap
