extends Area

func _physics_process(delta):
	$Visual.rotation.y += delta

func _on_PizzaPickup_body_entered(body):
	if(body.is_in_group("cab")):
		body.pizza += 1
		queue_free()
