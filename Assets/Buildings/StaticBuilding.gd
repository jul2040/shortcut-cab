extends Spatial

export(int) var size = 1
export(Color) var color
export(String) var global_name = ""
var rng

func generate():
	if(global_name != ""):
		GameManager.set(global_name, self)

func _on_VisibilityNotifier_screen_entered():
	visible = true

func _on_VisibilityNotifier_screen_exited():
	visible = false

func get_color():
	return color

func place_mission(m):
	m.global_transform.origin = $MissionPos.global_transform.origin
