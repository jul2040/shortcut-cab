extends Spatial

func ragdoll():
	for i in armature.get_children():
		if(i is PhysicalBone):
			i.get_child(0).disabled = false
	armature.physical_bones_start_simulation()
	$ScreamPlayer.play()

var hair_gradient = preload("res://Assets/Gradients/hair_colors.tres")
var skin_gradient = preload("res://Assets/Gradients/skin_colors.tres")
var pants_gradient = preload("res://Assets/Gradients/pants_colors.tres")
var shoe_gradient = preload("res://Assets/Gradients/shoe_colors.tres")
export(NodePath) var mesh_path
export(NodePath) var skeleton_path
export(Color) var shoe_color = Color()
var armature
func _ready():
	var c = Color(1, 0, 0)
	c.v = rand_range(0, 0.5)
	c.s = rand_range(0.5, 1)
	c.h = rand_range(0, 1)
	armature = get_node(skeleton_path)
	var body = get_node(mesh_path)
	body.get_surface_material(0).set_shader_param("color", c)
	var hair = hair_gradient.interpolate(randf())
	body.get_surface_material(0).set_shader_param("hair_color", hair)
	var skin = skin_gradient.interpolate(randf())
	body.get_surface_material(0).set_shader_param("skin_color", skin)
	var pants = pants_gradient.interpolate(randf())
	body.get_surface_material(0).set_shader_param("pants_color", pants)
	body.get_surface_material(0).set_shader_param("replace_shoe_color", shoe_color)
	var shoe = shoe_gradient.interpolate(randf())
	body.get_surface_material(0).set_shader_param("shoe_color", shoe)

export(String) var walk_animation = "Walking"
export(String) var wait_animation = "Waiting"
func walk():
	$AnimationPlayer.play(walk_animation)

func wait():
	$AnimationPlayer.play(wait_animation)

func speed_mult(m):
	$AnimationPlayer.playback_speed = m
