extends Camera

func _ready():
	print("TEST CAMERA")
	Input.set_mouse_mode(Input.MOUSE_MODE_CAPTURED)

var mouse = Vector2()
var speed = 30
func _physics_process(delta):
	var movement = Vector3()
	if(Input.is_action_pressed("up")):
		movement += -global_transform.basis.z
	if(Input.is_action_pressed("down")):
		movement += global_transform.basis.z
	if(Input.is_action_pressed("left")):
		movement += -global_transform.basis.x
	if(Input.is_action_pressed("right")):
		movement += global_transform.basis.x
	rotate_object_local(Vector3.LEFT, mouse.y)
	rotate(Vector3.UP, -mouse.x)
	mouse = Vector2()
	translation += movement*delta*speed

func _input(event):
	if(event is InputEventMouseMotion):
		mouse = event.relative*0.01
