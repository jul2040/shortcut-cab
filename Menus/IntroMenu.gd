extends Control

func _ready():
	return
	get_node("../PauseMenu").disable()
	get_node("../../ViewportContainer/Viewport/Hub").set_active(false)
	yield(get_tree(), "idle_frame")
	yield(get_tree(), "idle_frame")
	get_node("../../ViewportContainer/Viewport/Hub").set_active(true)
	get_tree().paused = true
	get_node("../PauseMenu").enable()
	update_controls()
	Input.set_mouse_mode(Input.MOUSE_MODE_VISIBLE)

func _on_Start_pressed():
	get_tree().paused = false
	Input.set_mouse_mode(Input.MOUSE_MODE_CAPTURED)
	queue_free()

var grapple = false
func set_grapple(g):
	grapple = g
	update_controls()

var rocket = false
func set_rocket(r):
	rocket = r
	update_controls()

func update_controls():
	$ControlLabel.text = "Controls\n%s%s%s%s - Drive\n%s - Drift\n"%[get_control("up"), get_control("left"), get_control("down"), get_control("right"), get_control("drift")]
	if(grapple):
		$ControlLabel.text += "%s - Grappling Hook\n"%get_control("grapple")
	if(rocket):
		$ControlLabel.text += "%s - Rocket\n"%get_control("rocket")

func get_control(action):
	return event_to_string(event_from_dict(SettingsSaver.get("keybinds")[action]))

func event_from_dict(d):
	if(d["type"] == "key"):
		var e = InputEventKey.new()
		e.set_scancode(d["scancode"])
		return e
	if(d["type"] == "mouse button"):
		var e = InputEventMouseButton.new()
		e.set_button_index(d["index"])
		return e

func event_to_string(event):
	if(event is InputEventKey):
		return OS.get_scancode_string(event.get_scancode())
	elif(event is InputEventMouseButton):
		return "Mouse Button "+str(event.button_index)
	return ""
