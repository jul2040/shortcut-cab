extends CanvasLayer

var actions = [["up", "Forwards"], ["down", "Backwards"], ["left", "Left"], ["right", "Right"], ["drift", "Drift"], ["interact", "Interact"], ["rocket", "Rocket Boost"], ["use equipped", "Use Equipped"], ["equip next", "Equip Next"], ["equip prev", "Equip Previous"], ["zoom in", "Zoom Camera In"], ["zoom out", "Zoom Camera Out"]]
var resolutions = [200, 480, 720, 768, 864, 900, 1080, 1440, 2160, 4320]
func _ready():
	_on_MusicSlider_value_changed(SettingsSaver.get("music"))
	$Settings/MusicSlider.value = SettingsSaver.get("music")
	_on_SFXSlider_value_changed(SettingsSaver.get("sfx"))
	$Settings/SFXSlider.value = SettingsSaver.get("sfx")
	for a in actions:
		#make menu item
		var s = preload("res://Menus/ControlSetting.tscn").instance()
		s.set_action(a)
		s.connect("reassign", self, "reassign_pressed")
		$Controls/ScrollContainer/VBoxContainer.add_child(s)
	for i in range(len(resolutions)):
		$Settings/ResolutionButton.add_item(str(resolutions[i])+"p", i)
	_on_ResolutionButton_item_selected(SettingsSaver.get("resolution"))
	$Settings/ResolutionButton.select(SettingsSaver.get("resolution"))

signal resolution_changed
func _on_ResolutionButton_item_selected(i):
	SettingsSaver.set("resolution", i)
	emit_signal("resolution_changed")

func get_resolution():
	return resolutions[SettingsSaver.get("resolution")]

var waiting_for_input = false
var setting
func reassign_pressed(s):
	waiting_for_input = true
	s.waiting()
	if(setting != null):
		setting.not_waiting()
	setting = s

func _input(event):
	if(waiting_for_input and (event is InputEventKey or event is InputEventMouseButton)):
		setting.assign_button(event)
		setting = null
		waiting_for_input = false

func _on_ExitControls_pressed():
	waiting_for_input = false
	if(setting != null):
		setting.not_waiting()
		setting = null
	$Controls.visible = false

func _on_ExitSettings_pressed():
	$Settings.visible = false

func set_visible(v):
	$Settings.visible = v

func get_visible():
	return $Settings.visible

func _on_SFXSlider_value_changed(value):
	AudioServer.set_bus_volume_db(AudioServer.get_bus_index("SFX"), linear2db(value))
	SettingsSaver.set("sfx", value)

func _on_MusicSlider_value_changed(value):
	AudioServer.set_bus_volume_db(AudioServer.get_bus_index("Music"), linear2db(value))
	SettingsSaver.set("music", value)

func _on_Controls_pressed():
	$Controls.visible = true

func pause_pressed():
	if($Controls.visible):
		$Controls.visible = false
		return
	$Settings.visible = false
