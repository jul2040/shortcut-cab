extends Control

var progression
var cur_ind = 0
func load_list(l):
	progression = l
	cur_ind = -1
	next()

func next():
	cur_ind += 1
	if(cur_ind <= len(progression)-1):
		var id = progression[cur_ind]["id"]
		var has = false
		for i in GameSaver.get("upgrades"):
			if(i == id):
				has = true
				break
		if(has):
			next()
		else:
			load_single(progression[cur_ind])
	else:
		queue_free()

var current
func load_single(s):
	$Label.text = s["name"]
	$CostLabel.text = str(s["cost"])
	current = s

func get_cost():
	if(current == null):
		return 0
	return current["cost"]

func get_action():
	if(current == null):
		return ["", 0]
	return current["action"]

signal buy(u)
func _on_Buy_pressed():
	emit_signal("buy", self)

func pizza_updated(p):
	$Buy.disabled = p < get_cost()

signal hover(info)
func _on_GarageUpgrade_mouse_entered():
	emit_signal("hover", current["info"])
