extends Control

func _ready():
	for i in [["fov", 80], ["music", 1], ["sfx", 1], ["rotate cam", true], ["invert y", false], ["sensitivity", 0.01]]:
		if(SettingsSaver.get(i[0]) == null):
			SettingsSaver.set(i[0], i[1])
	_on_FOVSlider_value_changed(SettingsSaver.get("fov"))
	$FOVSlider.value = SettingsSaver.get("fov")
	_on_RotateCam_toggled(SettingsSaver.get("rotate cam"))
	$RotateCam.pressed = SettingsSaver.get("rotate cam")
	_on_InvertY_toggled(SettingsSaver.get("invert y"))
	$InvertY.pressed = SettingsSaver.get("invert y")
	_on_MouseSensitivity_value_changed(SettingsSaver.get("sensitivity"))
	$MouseSensitivity.value = SettingsSaver.get("sensitivity")

var active = true
func enable():
	active = true

func disable():
	active = false

func _notification(what):
	if(active and what == MainLoop.NOTIFICATION_WM_FOCUS_OUT):
		if(get_tree().paused == visible):
			set_paused(true)

func set_paused(p):
	visible = p
	get_tree().paused = visible
	if(p):
		Input.set_mouse_mode(Input.MOUSE_MODE_VISIBLE)
	else:
		Input.set_mouse_mode(Input.MOUSE_MODE_CAPTURED)

func _process(_delta):
	if(active and Input.is_action_just_pressed("pause")):
		if(get_tree().paused == visible):
			if(SettingsMenu.get_visible()):
				SettingsMenu.pause_pressed()
			else:
				set_paused(not visible)

func _on_Resume_pressed():
	set_paused(false)

func _on_MainMenu_pressed():
	get_tree().paused = false
	Loader.goto_scene("res://Menus/MainMenu.tscn")

func _on_FOVSlider_value_changed(value):
	get_node("../../ViewportContainer/Viewport/Hub/Camera").fov = value
	SettingsSaver.set("fov", value)

func _on_RotateCam_toggled(button_pressed):
	get_node("../../ViewportContainer/Viewport/Hub").set_rotate_with_car(button_pressed)
	SettingsSaver.set("rotate cam", button_pressed)

func _on_InvertY_toggled(button_pressed):
	get_node("../../ViewportContainer/Viewport/Hub").set_invert_y(button_pressed)
	SettingsSaver.set("invert y", button_pressed)

func _on_MouseSensitivity_value_changed(value):
	get_node("../../ViewportContainer/Viewport/Hub").sensitivity = value
	SettingsSaver.set("sensitivity", value)

func _on_Fullscreen_pressed():
	OS.window_fullscreen = not OS.window_fullscreen

func _on_Controls_pressed():
	SettingsMenu.set_visible(true)
