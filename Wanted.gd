extends Control

var star_list
func _ready():
	star_list = get_children()

func set_stars(s):
	for i in star_list:
		i.visible = s > 0
		s -= 1
