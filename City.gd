extends Spatial

onready var cab = $Cab

var special_blocks = []
var block_ids = {0:preload("res://Blocks/Park.tscn")}
func new_block(x, y):
	for i in special_blocks:
		if(x+(y*x_size) == i[0]):
			return block_ids[i[1]].instance()
	if(rng.randi()%5 > 0):
		return preload("res://Blocks/Block.tscn").instance()
	else:
		return preload("res://Blocks/ModernBlock.tscn").instance()

func new_straight_street():
	return preload("res://Assets/Roads/StraightRoad.tscn").instance()

func new_intersection():
	return preload("res://Assets/Roads/Intersection.tscn").instance()

const BLOCK_SIZE = 5*20 # number of buildings * size of each building
const BUILDING_SIZE = 20
const STREET_WIDTH = 40
const WALL_THICKNESS = 10 # how thick the outer walls are (in terms of extents)
var x_size
var y_size
const MINIMAP_SCALE = 1
var minimap
var blocks = []
func generate(x_size, y_size):
	minimap = Image.new()
	minimap.create(x_size*(BLOCK_SIZE+STREET_WIDTH)+STREET_WIDTH/2, y_size*(BLOCK_SIZE+STREET_WIDTH)+STREET_WIDTH/2, false, Image.FORMAT_RGBA8)
	minimap.fill(Color(0.128906, 0.128906, 0.128906))
	minimap.lock()
	self.x_size = x_size
	self.y_size = y_size
	
	#place special blocks
	# [position, id]
	for i in [0]: # <-- list of ids
		#generate a position that isnt taken
		var done = false
		var position
		while not done:
			done = true
			position = rng.randi()%(x_size*y_size)
			for j in special_blocks:
				if(j[0]==position):
					done = false
					break
		special_blocks.append([position, i])
	
	var special_buildings = []
	#place special buildings
	# [position, id]
	for i in [0, 0, 0, 1]: # <-- list of ids
		#generate a position that isnt taken
		var done = false
		var position
		while not done:
			done = true
			position = rng.randi()%(x_size*y_size)
			for j in special_blocks: # make sure we dont put a building in a special block
				if(j[0]==position):
					done = false
					break
		special_buildings.append([position, i])
	var hideouts = []
	#place hideouts
	# [position, id]
	for i in [2, 3]: # <-- list of ids
		#generate a position that isnt taken
		var done = false
		var position
		while not done:
			done = true
			position = rng.randi()%(x_size*y_size)
			for j in special_blocks: # make sure we dont put a hideout in a special block
				if(j[0]==position):
					done = false
					break
			for j in hideouts: # dont put 2 hideouts on the same block
				if(j[0]==position):
					done = false
					break
		special_buildings.append([position, i])
		hideouts.append([position, i])
	#generate the blocks
	for x in range(x_size):
		blocks.append([])
		for y in range(y_size):
			var b = new_block(x, y)
			b.rng = rng
			b.translation = Vector3(x*(BLOCK_SIZE+STREET_WIDTH), 0, y*(BLOCK_SIZE+STREET_WIDTH))
			var cur = x+(y*x_size)
			for i in special_buildings:
				if(i[0] == cur):
					b.add_special_building(i[1])
			b.generate()
			add_child(b)
			blocks[x].append(b)
			#add to minimap
			var block = b.get_minimap()
			minimap.blit_rect(
				block,
				Rect2(Vector2(), block.get_size()), 
				(Vector2(x, y)*(BLOCK_SIZE+STREET_WIDTH))+Vector2(STREET_WIDTH/2, STREET_WIDTH/2))
	#Place the intersections
	for x in range(x_size+1):
		for y in range(y_size+1):
			var s = new_intersection()
			s.translation = Vector3(x*(BLOCK_SIZE+STREET_WIDTH), 0, y*(BLOCK_SIZE+STREET_WIDTH))
			s.translation -= Vector3(BUILDING_SIZE, 0, BUILDING_SIZE)
			add_child(s)
	#Place vertical streets
	for x in range(x_size+1):
		for y in range(1, y_size+1, 1):
			for y_mod in range(1, 5+2, 1):
				var s = new_straight_street()
				s.translation = Vector3(x*(BLOCK_SIZE+STREET_WIDTH), 0, y*(BLOCK_SIZE+STREET_WIDTH))
				s.translation -= Vector3(BUILDING_SIZE, 0, BUILDING_SIZE)
				s.translation.z -= y_mod*BUILDING_SIZE
				if(y_mod%4!=0):
					s.no_spawners()
				add_child(s)
	#Place horizontal streets
	for x in range(1, x_size+1, 1):
		for y in range(y_size+1):
			for x_mod in range(1, 5+2, 1):
				var s = new_straight_street()
				s.translation = Vector3(x*(BLOCK_SIZE+STREET_WIDTH), 0, y*(BLOCK_SIZE+STREET_WIDTH))
				s.translation -= Vector3(BUILDING_SIZE, 0, BUILDING_SIZE)
				s.translation.x -= x_mod*BUILDING_SIZE
				s.rotation.y += PI/2
				if(x_mod%4!=0):
					s.no_spawners()
				add_child(s)
	#Place the walls
	$"X+Wall/CollisionShape".shape.extents.x = WALL_THICKNESS
	$"X+Wall/CollisionShape".shape.extents.z = (BLOCK_SIZE+STREET_WIDTH)*y_size
	$"X+Wall".translation.x = (((BLOCK_SIZE+STREET_WIDTH)*x_size))
	
	$"X-Wall/CollisionShape".shape.extents.x = WALL_THICKNESS
	$"X-Wall/CollisionShape".shape.extents.z = (BLOCK_SIZE+STREET_WIDTH)*y_size
	$"X-Wall".translation.x = -STREET_WIDTH
	
	$"Z+Wall/CollisionShape".shape.extents.z = WALL_THICKNESS
	$"Z+Wall/CollisionShape".shape.extents.x = (BLOCK_SIZE+STREET_WIDTH)*x_size
	$"Z+Wall".translation.z = (((BLOCK_SIZE+STREET_WIDTH)*y_size))
	
	$"Z-Wall/CollisionShape".shape.extents.z = WALL_THICKNESS
	$"Z-Wall/CollisionShape".shape.extents.x = (BLOCK_SIZE+STREET_WIDTH)*x_size
	$"Z-Wall".translation.z = -STREET_WIDTH
	
	#Place the player in the center of the city
	cab.translation = Vector3((x_size/2)*(BLOCK_SIZE+STREET_WIDTH)-STREET_WIDTH/2, 0, (y_size/2)*(BLOCK_SIZE+STREET_WIDTH)-STREET_WIDTH/2)
	#Place and scale the floor
	$Floor.translation.x = (x_size/2)*(BLOCK_SIZE+STREET_WIDTH)+STREET_WIDTH
	$Floor.translation.z = (y_size/2)*(BLOCK_SIZE+STREET_WIDTH)+STREET_WIDTH
	$Floor/MeshInstance.mesh.size = Vector2(x_size*(BLOCK_SIZE+STREET_WIDTH), y_size*(BLOCK_SIZE+STREET_WIDTH))
	$Floor/CollisionShape.shape.extents.x = $Floor/MeshInstance.mesh.size.x/2
	$Floor/CollisionShape.shape.extents.z = $Floor/MeshInstance.mesh.size.y/2
	#Place the clouds
	for x in range(-1, x_size+1, 1):
		for y in range(-1, y_size+1, 1):
			if(x == -1 or y == -1 or x == x_size or y == y_size):
				var c = preload("res://Assets/Clouds.tscn").instance()
				var offset = Vector2(30, 30)
				if(x == -1):
					offset.x += STREET_WIDTH/2
					if(y==y_size):
						offset.y -= STREET_WIDTH
				if(y == -1):
					offset.y += STREET_WIDTH/2
					if(x==x_size):
						offset.x -= STREET_WIDTH
				if(x == x_size):
					offset.x += STREET_WIDTH/2
				if(y == y_size):
					offset.y += STREET_WIDTH/2
				if ((x == 0 and y == 0) or (x == 0 and y == y_size) or (x == x_size and y == 0) or (x == x_size and y == y_size)):
					offset = Vector2(30, 30)
				c.translation = Vector3((x)*(BLOCK_SIZE+STREET_WIDTH)+offset.x, -5, (y)*(BLOCK_SIZE+STREET_WIDTH)+offset.y)
				add_child(c)
	#place the initial passenger
	if(on_call):
		place_passenger()
	#minimap
	minimap.unlock()
	minimap.save_png("user://test.png")
	$Cab.set_minimap(minimap)

var passenger_pos
#places a passenger on the sidewalk
var passenger
func place_passenger():
	passenger = preload("res://Interactions/Passenger.tscn").instance()
	add_child(passenger)
	place(passenger, randf(), Vector2(randi()%(x_size), randi()%(y_size)))
	passenger_pos = passenger.translation
	passenger.level = self
	cab.set_target(passenger)

var trip_distance = 0
#places a dropoff on the sidewalk
var dropoff
var dropoff_offset = 0
var dropoff_block = Vector2()
func place_dropoff():
	dropoff = preload("res://Interactions/Dropoff.tscn").instance()
	add_child(dropoff)
	dropoff_offset = randf()
	dropoff_block = Vector2(randi()%(x_size), randi()%(y_size))
	place(dropoff, dropoff_offset, dropoff_block)
	#Use manhattan distance
	trip_distance = abs(passenger_pos.x-dropoff.translation.x)+abs(passenger_pos.z-dropoff.translation.z)
	dropoff.level = self
	cab.set_target(dropoff)

const SIDEWALK = 12
#Places an object at a place on the sidewalk
func place(object, r, block_coord):
	blocks[block_coord.x][block_coord.y].place(object, r)

var has_passenger = false
func passenger_picked():
	place_dropoff()
	MusicManager.play(0)
	has_passenger = true
	timer = 0

const BASE_FARE = 10 # amount that is given to the player no matter how long they take
const MAX_BONUS = 20 # max time bonus
const MAX_BONUS_TIME_PER_METER = 0.2 # in seconds/meter traveled
func dropped_off():
	place_passenger()
	blocks[dropoff_block.x][dropoff_block.y].place_pedestrian(cab.passenger, dropoff_offset)
	cab.passenger = null
	MusicManager.play(1)
	has_passenger = false
	var max_bonus_time = MAX_BONUS_TIME_PER_METER*trip_distance
	var bonus = int((max(-timer+max_bonus_time, 0)/max_bonus_time)*MAX_BONUS)
	cab.end_timer(bonus)
	cab.pizza += 10+bonus

func cancel_trip():
	if(is_instance_valid(dropoff)):
		dropoff.queue_free()
	MusicManager.play(1)
	has_passenger = false
	cab.end_timer(0)

const SUN_START = -90
const SUN_END = 270
const TIME_SCALE = 2
const DAY_LENGTH = (24.0*60.0) # in seconds
var sky_gradient = preload("res://Assets/Gradients/sky_gradient.tres")
var time = 12*60
var timer = 0
func _process(delta):
	if(has_passenger):
		timer += delta
		timer = timer
		cab.set_timer(timer)
	#day cycle
	time += delta*TIME_SCALE
	if(time > DAY_LENGTH):
		time -= DAY_LENGTH
	cab.set_clock(time)
	var sun_lat = ((SUN_END-SUN_START)/DAY_LENGTH)*time+SUN_START
	$DirectionalLight.rotation_degrees.x = 180+sun_lat
	$WorldEnvironment.environment.background_sky.sun_latitude = sun_lat
	if(sun_lat < 185 and sun_lat > -5):
		$DirectionalLight.light_energy = lerp($DirectionalLight.light_energy, 1, delta)
		if(lights):
			lights = false
			emit_signal("lights_off")
	else:
		$DirectionalLight.light_energy = lerp($DirectionalLight.light_energy, 0, delta)
		if(not lights):
			lights = true
			emit_signal("lights_on")

var rng
func _ready():
	rng = RandomNumberGenerator.new()
	rng.set_seed(GameSaver.get("city seed"))
	GameManager.level = self
	var size = GameManager.city_sizes[SettingsSaver.get("city size")]
	generate(size, size)
	_on_SkyTimer_timeout()
	MusicManager.play(1)
	cab.connect("pizza_updated", self, "pizza_updated")
	mission_place()

func add_car(car, spawner):
	add_child(car)
	car.player = cab
	spawner.place(car)

var lights = false
signal lights_on
signal lights_off
func _on_SkyTimer_timeout():
	var sky_color = sky_gradient.interpolate(float(time)/DAY_LENGTH)
	$WorldEnvironment.get_environment().get_sky().sky_top_color = sky_color
	sky_color.v *= 0.25
	$WorldEnvironment.get_environment().get_sky().sky_horizon_color = sky_color
	$WorldEnvironment.get_environment().get_sky().ground_horizon_color = sky_color
	sky_color.v *= 0.1
	$WorldEnvironment.get_environment().get_sky().ground_bottom_color = sky_color

signal on_call_toggled(c)
var on_call = false
func toggle_on_call():
	on_call = not on_call
	if(on_call):
		place_passenger()
	else:
		if(has_passenger):
			cancel_trip()
		else:
			passenger.queue_free()
		cab.set_target(null)
	emit_signal("on_call_toggled")

func add_poi(p):
	cab.add_poi(p)

func remove_poi(p):
	cab.remove_poi(p)

var mission
func mission_place():
	var index = GameSaver.get("mission")
	if(index >= GameManager.missions.size()):
		return
	mission = GameManager.missions[index]
	var start
	if(mission["interact"]):
		start = preload("res://Mission/InteractStart.tscn").instance()
	else:
		start = preload("res://Mission/NoInteractStart.tscn").instance()
	add_child(start)
	GameManager.get(mission["location"]).place_mission(start)

enum {NO_MISSION, PIZZA, GOTO, CUTSCENE, KILL_TARGET}
var mission_state = NO_MISSION
var mission_pizza = 0
var goto_places = []
func mission_start():
	cab.mission = true
	if(mission["memo"] != null):
		cab.set_memo(mission["memo"])
	match mission["type"]:
		"pizza":
			mission_state = PIZZA
			mission_pizza = 0
			cab.set_pizza_mission(true)
			if(not on_call):
				toggle_on_call()
		"goto":
			mission_state = GOTO
			goto_places = mission["places"]
			place_goto()
		"cutscene":
			mission_state = CUTSCENE
		"kill target":
			mission_state = KILL_TARGET
			var p = preload("res://Pedestrians/TargetPedestrian.tscn").instance()
			blocks[randi()%x_size][randi()%y_size].place_path_follow(p)
			cab.set_target(p)
			MusicManager.play(0)
	if(mission["start message"] != null):
		cab.show_dialogue(mission["start message"])

func target_killed():
	cab.set_target(null)
	MusicManager.play(1)
	mission_end()

func place_goto():
	var goto = preload("res://Mission/Goto.tscn").instance()
	add_child(goto)
	GameManager.get(goto_places[0]["location"]).place_mission(goto)
	cab.set_target(goto)
	if(goto_places[0]["memo"] != null):
		cab.set_memo(goto_places[0]["memo"])

func goto_destination_reached():
	if(goto_places[0]["message"] != null):
		cab.show_dialogue(goto_places[0]["message"])
	goto_places.remove(0)
	if(goto_places.size() > 0):
		place_goto()
	else:
		mission_end()

func pizza_updated(p):
	if(mission_state == PIZZA):
		if(p >= mission["amount"]):
			toggle_on_call()
			mission_end()

func mission_end():
	$MissionEndTimer.start()

func _on_MissionEndTimer_timeout():
	cab.mission = false
	cab.set_memo("")
	cab.show_dialogue(mission["end message"])
	mission_state = NO_MISSION
	cab.set_pizza_mission(false)
	cab.pizza += mission["reward"]
	GameSaver.set("mission", GameSaver.get("mission")+1)
	mission_place()

func arrest_cab():
	cab.set_stars(0)
	cab.linear_velocity = Vector3()
	cab.angular_velocity = Vector3()
	GameManager.police_hq.place_mission(cab)
	cab.pizza = int(cab.pizza*0.5)
