extends PathFollow

func new_passenger():
	if(randi()%2 == 0):
		return preload("res://Assets/Passenger.tscn").instance()
	else:
		return preload("res://Assets/FemalePassenger.tscn").instance()

export(Vector2) var speed_range = Vector2(1, 2)
var direction = 1
var passenger
func _ready():
	$ForwardRay.add_exception($PedestrianBody)
	if(passenger == null):
		passenger = new_passenger()
		add_child(passenger)
	else:
		passenger.translation = Vector3()
		add_child(passenger)
	if(randi()%2 == 0): # turn around
		passenger.rotation.y += PI
		$ForwardRay.rotation.y += PI
		$ForwardRay.collision_mask = 1+32
		$PedestrianBody.collision_layer = 32+128
		$PedestrianBody.collision_mask = $PedestrianBody.collision_layer
		direction = -1
	speed_mult = rand_range(speed_range.x, speed_range.y)
	passenger.speed_mult(speed_mult)
	passenger.walk()
	if(not remove_if_far):
		$PedestrianBody.queue_free()

var cab
const WALK_SPEED = 1.5
var speed_mult = 1
export(bool) var remove_if_far = true
func _physics_process(delta):
	if(not $ForwardRay.is_colliding()):
		passenger.walk()
		offset += delta*WALK_SPEED*direction*speed_mult
	else:
		passenger.wait()
	if(remove_if_far and global_transform.origin.distance_to(cab.global_transform.origin) > 100):
		queue_free()

export(float) var stars = 0.5
func _on_RagdollArea_body_entered(body):
	if(body.is_in_group("physics")):
		ragdoll()
		if(body.is_in_group("cab")):
			body.set_stars(body.stars+stars)

func ragdoll():
	passenger.ragdoll()
	if(is_instance_valid($RagdollArea)):
		$RagdollArea.queue_free()
	if(is_instance_valid(get_node("PedestrianBody"))):
		$PedestrianBody.queue_free()
	$RagdollTimer.start()
	direction = 0
	killed()

func killed():
	pass

func _on_RagdollTimer_timeout():
	queue_free()

func damage():
	ragdoll()
	GameManager.level.cab.set_stars(GameManager.level.cab.stars+stars)
