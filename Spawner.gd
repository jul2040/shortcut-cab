extends Area

func _ready():
	setup_parent()

func setup_parent():
	parent = GameManager.level

func _physics_process(delta):
	attempt_spawn()

var parent = self
func attempt_spawn():
	if(bodies_inside <= 0 and len(spawn_queue) > 0):
		var body = spawn_queue[len(spawn_queue)-1]
		spawn_queue.remove(len(spawn_queue)-1)
		if(not is_instance_valid(body)):
			return
		parent.call_deferred("add_car", body, self)

var waiting_for_physics = 0
func place(body):
	body.global_transform.origin = global_transform.origin
	body.rotation = global_transform.basis.get_euler()
	bodies_inside += 1
	waiting_for_physics += 1

func new_car():
	if(randi()%6 < GameManager.level.cab.stars):
		return preload("res://Cars/PoliceCar.tscn").instance()
	var r = randi()%3
	if(r == 0):
		return preload("res://Cars/Car.tscn").instance()
	elif(r==1):
		return preload("res://Cars/Truck.tscn").instance()
	else:
		return preload("res://Cars/Bus.tscn").instance()

var spawn_queue = []
func spawn():
	var s = new_car()
	spawn_queue.append(s)
	attempt_spawn()

var bodies_inside = 0
func _on_Spawner_body_entered(body):
	if(body.is_in_group("physics")):
		if(waiting_for_physics <= 0):
			bodies_inside += 1
		else:
			waiting_for_physics -= 1

func _on_Spawner_body_exited(body):
	if(body.is_in_group("physics")):
		bodies_inside -= 1

func reset():
	var size = len(spawn_queue)
	for i in spawn_queue:
		if(is_instance_valid(i)):
			i.queue_free()
	spawn_queue = []
	return size
