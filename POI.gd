extends Sprite

func set_color(c):
	$ColorRect.color = c

func set_size(s):
	scale = Vector2(s, s)
