extends Spatial

export(bool) var show_offscreen = false
export(Color) var color = Color()
export(float) var size = 1
export(bool) var show_during_mission = false

func _ready():
	if(is_instance_valid(GameManager.level)):
		GameManager.level.add_poi(self)

func _exit_tree():
	if(is_instance_valid(GameManager.level)):
		GameManager.level.remove_poi(self)
