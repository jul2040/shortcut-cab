extends TextureRect

func set_speed(s):
	$Needle.rect_rotation = clamp((s*3)-116, -116, 116)+180
	$Label.text = str(int(s*1.5))
