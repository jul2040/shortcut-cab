extends Control

var queue = []
# {"name":"PERSONS's NAME", "text":"MESSAGE"}
func display(d):
	if(visible):
		queue.append(d)
		return
	Input.set_mouse_mode(Input.MOUSE_MODE_VISIBLE)
	get_tree().paused = true
	$NameLabel.text = d["name"]
	$MessageLabel.visible_characters = 0
	$MessageLabel.text = d["text"]
	visible = true
	$CharacterTimer.start()

func _on_CharacterTimer_timeout():
	$MessageLabel.visible_characters += 1
	if($MessageLabel.visible_characters >= $MessageLabel.text.length()):
		$CharacterTimer.stop()

func _input(event):
	if(not visible):
		return
	if(event is InputEventMouseButton and event.is_pressed()):
		clicked()
	if(Input.is_action_just_pressed("pause") or Input.is_action_just_pressed("rocket") or Input.is_action_just_pressed("interact")):
		clicked()

func clicked():
	if($MessageLabel.visible_characters < $MessageLabel.text.length()):
		$MessageLabel.visible_characters = $MessageLabel.text.length()
	else:
		if(queue.size() > 0):
			var d = queue[0]
			$NameLabel.text = d["name"]
			$MessageLabel.visible_characters = 0
			$MessageLabel.text = d["text"]
			$CharacterTimer.start()
			queue.remove(0)
		else:
			visible = false
			Input.set_mouse_mode(Input.MOUSE_MODE_CAPTURED)
			get_tree().paused = false
